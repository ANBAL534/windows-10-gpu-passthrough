#!/usr/bin/env bash
# This script dynamically manages allocated hugepages size depending on running libvirt VMs.
# Based on Thomas Lindroth's shell script which sets up host for VM: http://sprunge.us/JUfS
# put this script to /etc/libvirt/hooks/qemu

VM_G_RAM=6                     # How many GBs of RAM the VM needs
VM_NEEDED_MEMORY=$((VM_G_RAM*(2**30)))
TOTAL_CORES='0-11'
TOTAL_CORES_MASK=FFF            # 0-11, bitmask 0b111111111111
HOST_CORES='0-1,6-7'            # Cores reserved for host
HOST_CORES_MASK=C3              # 0-1,6-7, bitmask 0b000011000011
VIRT_CORES='2-5,8-11'           # Cores reserved for virtual machine(s)

#HUGEPAGES_SIZE=$(grep Hugepagesize /proc/meminfo | awk {'print $2'})
#HUGEPAGES_SIZE=$((HUGEPAGES_SIZE * 1024))
#HUGEPAGES_ALLOCATED=$(sysctl vm.nr_hugepages | awk {'print $3'})

VM_NAME=$1
VM_ACTION=$2

shield_vm() {
    cset set -c $TOTAL_CORES -s machine.slice
    # Shield two cores cores for host and rest for VM(s)
    # cset shield --kthread on --cpu $VIRT_CORES    # THIS WILL FAIL, WE HAVE TO IMPLEMENT SHIELDS WITH set/proc
    cset set -c $VIRT_CORES -s win10-shielded       # Create user and system sets
    cset set -c $HOST_CORES -s host-shielded
    cset proc -m -f root -t host-shielded
    cset proc -k -f root -t host-shielded            # Move userspace and kernel cpu-unbound tasks to system
    echo 0 > /sys/fs/cgroup/cpuset/system/cpuset.cpu_exclusive
    echo 0 > /sys/fs/cgroup/cpuset/user/cpuset.cpu_exclusive
}

unshield_vm() {
    echo $TOTAL_CORES_MASK > /sys/bus/workqueue/devices/writeback/cpumask
    #cset shield --reset
    cset set -d win10-shielded
    cset set -d host-shielded
}

# For manual invocation
if [[ $VM_NAME == 'shield' ]];
then
    shield_vm
    exit 0
elif [[ $VM_NAME == 'unshield' ]];
then
    unshield_vm
    exit 0
elif [[ $VM_NAME != 'win10' ]];
then
    exit 0
fi

#cd $(dirname "$0")
#VM_HUGEPAGES_NEED=$(( $(./vm-mem-requirements $VM_NAME) / HUGEPAGES_SIZE ))
#VM_HUGEPAGES_NEED=$(( VM_NEEDED_MEMORY / HUGEPAGES_SIZE ))

if [[ $VM_ACTION == 'prepare' ]];
then
    virsh net-start default    # Turn on networking if not already on
    #java -jar /run/media/anibal/1TeraByteE/Proyectos/VM-Broadcaster/out/artifacts/VM_Broadcaster_jar/VM-Broadcaster.jar -l

    # Start audio
    sudo systemctl start scream-ivshmem-pulse.service

    # Prepare virtual screen
    sudo -u anibal xrandr --addmode VIRTUAL1 1920x1080
    sudo -u anibal xrandr

    sync
    echo 3 > /proc/sys/vm/drop_caches
    echo 1 > /proc/sys/vm/compact_memory
    
    echo "Waiting 3s for cache to drop"
    sleep 3s

    #VM_HUGEPAGES_TOTAL=$(($HUGEPAGES_ALLOCATED + $VM_HUGEPAGES_NEED))
    #sysctl vm.nr_hugepages=$VM_HUGEPAGES_TOTAL

    shield_vm
    # Reduce VM jitter: https://www.kernel.org/doc/Documentation/kernel-per-CPU-kthreads.txt
    sysctl vm.stat_interval=120

    sysctl -w kernel.watchdog=0
    # the kernel's dirty page writeback mechanism uses kthread workers. They introduce
    # massive arbitrary latencies when doing disk writes on the host and aren't
    # migrated by cset. Restrict the workqueue to use only cpu 0.
    echo $HOST_CORES_MASK > /sys/bus/workqueue/devices/writeback/cpumask
    # THP can allegedly result in jitter. Better keep it off.
    echo never > /sys/kernel/mm/transparent_hugepage/enabled
    # Force P-states to P0
    #echo performance | tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor
    #pstate-frequency --set -p max
    echo 0 > /sys/bus/workqueue/devices/writeback/numa
    >&2 echo "VMs Shielded"
    
    echo "Performing minor optimizations prior to launch..."
    echo -1 > /proc/sys/kernel/sched_rt_runtime_us
    for i in $(pgrep '^rcuo'); do taskset -pc 0,6 $i > /dev/null; done;
    for i in /sys/devices/virtual/workqueue/*/cpumask; do echo 041 > $i; done;
fi

if [[$VM_ACTION == 'started']];
then
    QEMUPID=$(pidof --single-shot qemu-system-x86_64)
    QEMUGROUPID=$(/bin/ps -Lo pgid --no-heading --pid $QEMUPID | sort --unique)
    renice --priority -13 --pgrp $QEMUGROUPID
    renice --priority -15 --pid $QEMUPID
    taskset --cpu-list --all-tasks --pid 0-3 $QEMUPID
    
    chrt -a -f -p 99 $(pidof qemu-system-x86_64)
    echo "Set QEMU execution policy!"
    chrt -p $(pidof qemu-system-x86_64)

    echo "Setting IRQ affinities..."
    bash -c "for i in $(sed -n -e 's/ \([0-9]\+\):.*/\1/p' /proc/interrupts); do echo '0,6' > /proc/irq/$i/smp_affinity_list; done;" > /dev/null 2>&1
fi

if [[ $VM_ACTION == 'release' ]];
then
    #VM_HUGEPAGES_TOTAL=$(($HUGEPAGES_ALLOCATED - $VM_HUGEPAGES_NEED))
    #VM_HUGEPAGES_TOTAL=$(($VM_HUGEPAGES_TOTAL<0?0:$VM_HUGEPAGES_TOTAL))
    #sysctl vm.nr_hugepages=$VM_HUGEPAGES_TOTAL

    # Stop audio
    sudo systemctl stop scream-ivshmem-pulse.service

    #All VMs offline
    echo fff > /sys/devices/virtual/workqueue/writeback/cpumask
    echo 950000 > /proc/sys/kernel/sched_rt_runtime_us
    sysctl vm.stat_interval=1
    sysctl -w kernel.watchdog=1
    unshield_vm
    echo always > /sys/kernel/mm/transparent_hugepage/enabled
    #echo powersave | tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor
    echo 1 > /sys/bus/workqueue/devices/writeback/numa
    >&2 echo "VMs UnShielded"
fi