## Configuration files for a working KVM Qemu libvirt GPU-Passthrough.

This are the main configuration and needed files for a working GPU Passthrough based and iterated on multiples guides found online and the combination on the knowledge they provided:

* [https://wiki.archlinux.org/index.php/PCI_passthrough_via_OVMF](https://wiki.archlinux.org/index.php/PCI_passthrough_via_OVMF) 
* [https://passthroughpo.st/simple-per-vm-libvirt-hooks-with-the-vfio-tools-hook-helper/](https://passthroughpo.st/simple-per-vm-libvirt-hooks-with-the-vfio-tools-hook-helper/)
* [https://rokups.github.io/#!pages/gaming-vm-performance.md](https://rokups.github.io/#!pages/gaming-vm-performance.md)
* [https://www.reddit.com/r/VFIO/comments/cieph2/update_to_another_dpc_latency_post_success_with/](https://www.reddit.com/r/VFIO/comments/cieph2/update_to_another_dpc_latency_post_success_with/)
* [https://passthroughpo.st/using-evdev-passthrough-seamless-vm-input/](https://passthroughpo.st/using-evdev-passthrough-seamless-vm-input/) 
* [https://forums.guru3d.com/threads/windows-line-based-vs-message-signaled-based-interrupts-msi-tool.378044/](https://forums.guru3d.com/threads/windows-line-based-vs-message-signaled-based-interrupts-msi-tool.378044/)
* [https://www.reddit.com/r/VFIO/comments/f2ypgz/5700xt_use_amd_gpu_on_host_and_vm_rebind_drivers/](https://www.reddit.com/r/VFIO/comments/f2ypgz/5700xt_use_amd_gpu_on_host_and_vm_rebind_drivers/)
* [https://www.reddit.com/r/VFIO/comments/991qzz/solutions_for_bindingunbinding_gpu_from_host/](https://www.reddit.com/r/VFIO/comments/991qzz/solutions_for_bindingunbinding_gpu_from_host/)

## Target hardware:

 - **Motherboard**: MSI MPG Z390 GAMING EDGE AC (MS-7B17) rev2.0
 - **CPU**: Intel(R) Core(TM) i7-8700K (OC to 4.8GHz)
 - **RAM**: 31972 MB @ 2400MHz (DIMM DDR4)
 - **Host GPU**: MSI Radeon RX 580 Armor OC & Intel CometLake-S GT2 (UHD Graphics 630)
 - **Guest GPU**: MSI Radeon RX 580 Armor OC


## Important Notes:

 - **Host**: Arch Linux
 - **Kernel**: linux-vfio, IOMMU Groups are fucked up in the motherboard/CPU
 - **Windows 10 version**: 1803
 - **GPU Usage**: In host use DRI_PRIME=1 to use the dGPU so you can pass it to the guest when unused in the host.
 - **AMD GPU Users**: Renember to use [https://github.com/gnif/vendor-reset](https://github.com/gnif/vendor-reset)
